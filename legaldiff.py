#!/usr/bin/env python3

import TexSoup

import argparse
import difflib
import hashlib
import re
import sys

parser = argparse.ArgumentParser(description='Compute a diff between two latex-legal files.')
parser.add_argument('file1')
parser.add_argument('file2')

args = parser.parse_args()

# Parse the input files
with open(args.file1, 'r') as f:
	soup1 = TexSoup.TexSoup(f)
with open(args.file2, 'r') as f:
	soup2 = TexSoup.TexSoup(f)

# Parse input file TOCs
with open(args.file1.replace('.tex', '.toc'), 'r') as f:
	toc1 = TexSoup.TexSoup(f)
with open(args.file2.replace('.tex', '.toc'), 'r') as f:
	toc2 = TexSoup.TexSoup(f)

def get_number(toc, counter):
	for contentsline in toc.find_all('contentsline'):
		if contentsline.args[3].value == counter:
			idx_start = contentsline.args[1].value.index('\\numberline{') + 12
			idx_end = contentsline.args[1].value.index('}', idx_start)
			return contentsline.args[1].value[idx_start:idx_end]
	return None

# Data model
class LegalNode:
	def __init__(self, name):
		self.name = name
		self.children = []
		self._hash = None
	
	def __repr__(self):
		return '<{} "{}">'.format(self.__class__.__name__, self.name)
	
	@property
	def hash(self):
		if self._hash:
			return self._hash
		m = hashlib.sha1()
		m.update(self.name.encode('utf-8'))
		for child in self.children:
			if isinstance(child, LegalNode):
				m.update(child.hash)
			else:
				m.update(child.encode('utf-8'))
		return m.digest()

class LegalPart(LegalNode):
	def __init__(self, level, name):
		super().__init__(name)
		self.level = level
		self.counter = None
	
	def describe_level(self, capitalise=False):
		return self.level[0].upper() + self.level[1:]
	
	def describe(self, toc, capitalise=False):
		return self.describe_level(capitalise) + ' ' + get_number(toc, self.counter)
	
	def stringify_heading(self, toc):
		return '\\textbf{' + self.level[0].upper() + self.level[1:] + ' ' + get_number(toc, self.counter) + '—' + self.name + '}'
	
	def stringify(self, toc):
		return self.stringify_heading(toc) + '\n\n' + ''.join([child.stringify(toc) if isinstance(child, LegalNode) else child for child in self.children])

class LegalSection(LegalNode):
	def __init__(self, name):
		super().__init__(name)
		self.counter = None
	
	def describe_level(self, capitalise=False):
		return 'Section' if capitalise else 'section'
	
	def describe(self, toc, capitalise=False):
		return self.describe_level(capitalise) + ' ' + get_number(toc, self.counter)
	
	def stringify_heading(self, toc):
		return '\\textbf{\\makebox[\\lmarg][l]{' + get_number(toc, self.counter) + '}' + self.name + '}'
	
	def stringify(self, toc):
		return self.stringify_heading(toc) + '\n\n' + ''.join([child.stringify(toc) if isinstance(child, LegalNode) else child for child in self.children])

# Build a tree of all the section contents
def build_outline(soup):
	outline = []
	counters = {'part': 0, 'division': 0, 'section': 0}
	for child in soup.document.contents:
		if isinstance(child, TexSoup.TexNode) and child.name in ['part', 'division', 'section']:
			if child.name == 'section':
				node = LegalSection(child.string)
				parent = next((x for x in reversed(outline) if isinstance(x, LegalPart)), None)
				if parent:
					parent.children.append(node)
				else:
					outline.append(node)
			elif child.name == 'division':
				node = LegalPart(child.name, child.string)
				parent = next((x for x in reversed(outline) if isinstance(x, LegalPart) and x.level == 'part'), None)
				if parent:
					parent.children.append(node)
				else:
					outline.append(node)
			elif child.name == 'part':
				node = LegalPart(child.name, child.string)
				outline.append(node)
			
			# Impute counters
			if child.name == 'part':
				counters['division'] = 0
			counters[child.name] += 1
			if child.name == 'division':
				node.counter = 'division.' + str(counters['part']) + '.' + str(counters['division'])
			else:
				node.counter = child.name + '.' + str(counters[child.name])
		else:
			if len(outline) > 0:
				parent = outline[-1]
				if isinstance(parent, LegalNode):
					while isinstance(parent, LegalNode) and parent.children and isinstance(parent.children[-1], LegalNode):
						parent = parent.children[-1]
					if parent.children and isinstance(parent.children[-1], str):
						parent.children[-1] += str(child)
					else:
						parent.children.append(str(child))
				else:
					outline[-1] += str(child)
			else:
				outline.append(str(child))
	
	return outline

outline1 = build_outline(soup1)
outline2 = build_outline(soup2)

# Produce the amendments
def fmt(fstr, *args, **kwargs):
	return fstr.replace('{', '{{').replace('}', '}}').replace('<', '{').replace('>', '}').format(*args, **kwargs)

def walk_diff_node(node1, node2):
	matcher = difflib.SequenceMatcher(None, [x.hash if isinstance(x, LegalNode) else x for x in node1.children], [x.hash if isinstance(x, LegalNode) else x for x in node2.children])
	opcodes = matcher.get_opcodes()
	
	if node1.name != node2.name:
		# Rename
		if any(x[0] != 'equal' for x in opcodes):
			# Rename and replace
			print(fmt('\\section{<location>}\nRepeal the <level>, substitute:\n\n\\begin{quotation}<content>\\end{quotation}\n', location=node1.describe(toc1, True), level=node1.describe_level(), content=node2.stringify(toc2)))
		else:
			# Rename only
			print(fmt('\\section{<location> (heading)}\nRepeal the heading, substitute:\n\n\\begin{quotation}<content>\\end{quotation}\n', location=node1.describe(toc1, True), content=node2.stringify_heading(toc2)))
	else:
		# Replace only
		#if sum(x[2]-x[1] for x in opcodes if x[0] == 'equal') / opcodes[-1][2] < 0.5:
		if False:
			# Replace whole thing
			print(fmt('\\section{<location>}\nRepeal the <level>, substitute:\n\n\\begin{quotation}<content>\\end{quotation}\n', location=node1.describe(toc1, True), level=node1.describe_level(), content=node2.stringify(toc2)))
		else:
			if isinstance(node1, LegalSection):
				# Try to diff words
				words1 = re.sub(r'\s+', r' ', ''.join(node1.children)).split(' ')
				words2 = re.sub(r'\s+', r' ', ''.join(node2.children)).split(' ')
				
				matcher = difflib.SequenceMatcher(None, words1, words2)
				opcodes = matcher.get_opcodes()
				
				if sum(1 for x in opcodes if x[0] != 'equal') <= 1:
					# Single
					tag, i1, i2, j1, j2 = next(x for x in opcodes if x[0] != 'equal')
					if tag == 'insert':
						raise Exception('TODO') # FIXME
					elif tag == 'delete':
						raise Exception('TODO') # FIXME
					elif tag == 'replace':
						print(fmt('\\section{<location>}\nOmit ‘<omission>’, substitute ‘<substitution>’.\n', location=node1.describe(toc1, True), omission=' '.join(words1[i1:i2]), substitution=' '.join(words2[j1:j2])))
				else:
					# Replace whole thing
					print(fmt('\\section{<location>}\nRepeal the <level>, substitute:\n\n\\begin{quotation}<content>\\end{quotation}\n', location=node1.describe(toc1, True), level=node1.describe_level(), content=node2.stringify(toc2)))
			else:
				# Descend
				walk_diff(node1.children, node2.children)

def walk_diff(tree1, tree2):
	matcher = difflib.SequenceMatcher(None, [x.hash if isinstance(x, LegalNode) else x for x in tree1], [x.hash if isinstance(x, LegalNode) else x for x in tree2])
	opcodes = matcher.get_opcodes()
	
	n = 0
	while True:
		if n >= len(opcodes):
			break
		
		#print(opcodes[n], file=sys.stderr)
		tag, i1, i2, j1, j2 = opcodes[n]
		
		if tag == 'insert':
			for j in range(j1, j2):
				j_elem = tree2[j]
				if i1 > 0:
					i_prev_elem = tree1[i1-1]
					print(fmt('\\section{After <location>}\nInsert:\n\n\\begin{quotation}<content>\\end{quotation}\n', location=i_prev_elem.describe(toc1), content=j_elem.stringify(toc2)))
				else:
					raise Exception('TODO') # FIXME: Inserting at the beginning of part, etc.
		elif tag == 'delete':
			for i in range(i1, i2):
				i_elem = tree1[i]
				print(fmt('\\section{<location>}\nRepeal the <level>.\n', location=i_elem.describe(toc1, True), level=i_elem.describe_level()))
		elif tag == 'replace':
			walk_diff_node(tree1[i1], tree2[j1])
			
			if j2-j1 > i2-i1:
				# Extra additions
				extra = (j2-j1) - (i2-i1)
				opcodes.insert(n+1, ('insert', i2, i2, j2 - extra, j2))
			
			if i2-i1 > 1:
				# Split this opcode
				opcodes.insert(n+1, ('replace', i1+1, i2, j1+1, j2))
		
		n += 1

walk_diff(outline1, outline2)

#import pdb; pdb.set_trace(); ...
