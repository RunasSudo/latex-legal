% Copyright (C) 2018-2019  RunasSudo (Yingtong Li)
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{legal}[2019/05/21 Legal documents class]

\LoadClass[a4paper,12pt]{article}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\newlength\lmarg
\setlength{\lmarg}{1cm}

\newcommand{\headingsize}{\fontsize{13pt}{15pt}\selectfont}

% Configuration
\RequirePackage[top=1.25cm,bottom=1.13cm,inner=\dimexpr 2cm + \lmarg\relax,outer=2cm,headheight=8pt,headsep=0.5cm,footskip=1cm,includehead,includefoot]{geometry}
\frenchspacing
\RequirePackage[hidelinks,bookmarksnumbered=true]{hyperref}
\RequirePackage{bookmark}
\RequirePackage{parskip}
\setlength{\parskip}{0.35cm plus 0.1cm minus 0.1cm}
\RequirePackage{fancyhdr}
\setlength{\emergencystretch}{3em}

% PDF/A compliance
\providecommand{\xmp@Title}{\mytitle}
\providecommand{\xmp@Author}{\myauthor}
\providecommand{\xmp@CopyrightURL}{https://creativecommons.org/licenses/by-sa/4.0/}
\RequirePackage[a-3u]{pdfx}

\RequirePackage{microtype}

% Fonts
\usepackage[math-style=ISO, bold-style=ISO]{unicode-math}
\setmainfont[RawFeature=-tlig]{TeX Gyre Termes}
\setsansfont[RawFeature=-tlig]{TeX Gyre Heros}
\setmonofont[RawFeature=-tlig]{TeX Gyre Cursor}
\setmathfont[RawFeature=-tlig]{TeX Gyre Termes Math}
\renewcommand{\familydefault}{\sfdefault}

% Heading formats
\RequirePackage[loadonly,newparttoc,aftersep]{titlesec}
\RequirePackage{titletoc}

\let\headnum\arabic

\newcounter{toc@section} % For some reason \newif doesn't work here
\newcommand{\toc@sectiontrue}{\setcounter{toc@section}{1}}
\newcommand{\toc@sectionfalse}{\setcounter{toc@section}{0}}

\titleclass{\part}[0]{straight}
\def\thepart{\texorpdfstring{\headnum{part}}{Part \headnum{part}—\ignorespaces}}
\titleformat{\part}{\bfseries\headingsize\centering\uppercase}{\hspace*{-0.5\lmarg}Part \thepart{}—}{0pt}{}
\titlespacing{\part}{-\lmarg}{1cm plus 0.3cm minus 0.3cm}{0.64cm plus 0.2cm minus 0.2cm}
\titlecontents{part}[0pt]{\vspace{0.21cm}\toc@sectionfalse\bfseries}{\hspace*{-\lmarg}PART \thecontentslabel{}—\uppercase}{}{\titlerule*[1pc]{.}\contentspage}[]
\def\toclevel@part{0}

\titleclass{\division}{straight}[\part]
\newcounter{division}[part]
\def\thedivision{\texorpdfstring{\headnum{division}}{Division \headnum{division}—\ignorespaces}}
\titleformat{\division}{\bfseries\headingsize\centering}{\hspace*{-0.5\lmarg}Division \thedivision{}—}{0pt}{}
\titlespacing{\division}{-\lmarg}{1cm plus 0.3cm minus 0.3cm}{0.64cm plus 0.2cm minus 0.2cm}
\titlecontents{division}[0pt]{\vspace{0.21cm}\bfseries\scshape}{\toc@sectionfalse\hspace*{-\lmarg}Division \thecontentslabel{}—}{}{\titlerule*[1pc]{.}\contentspage}[]
\def\toclevel@division{1}

\titleclass{\presection}{straight}[\division]
\def\toclevel@presection{1}

\titleclass{\section}{straight}[\presection]
\def\thesection{\headnum{section}}
\titleformat{\section}{\bfseries}{\makebox[\lmarg][l]{\thesection}}{0pt}{}
\titlespacing{\section}{-\lmarg}{0.64cm plus 0.1cm minus 0.1cm}{0.35cm plus 0.1cm minus 0.1cm}
\dottedcontents{section}[0.5cm]{\ifnum\value{toc@section}=0\vspace{0.21cm}\fi\toc@sectiontrue}{1cm}{1pc}[]
\def\toclevel@section{2}

\titleclass{\sectiondiv}{straight}[\section]
\newcounter{sectiondiv}[section]
\def\thesectiondiv{}
\titleformat{\sectiondiv}{\itshape\setlength{\hangindent}{0cm}}{}{0pt}{}
\titlespacing{\sectiondiv}{0pt}{\parskip}{\parskip}
\titlecontents{sectiondiv}[0.5cm]{}{}{}{\hspace{\fill}\contentspage}[]
\def\toclevel@sectiondiv{3}

%\setcounter{tocdepth}{4}
\setcounter{tocdepth}{3}

% Table of contents heading
\renewcommand{\tableofcontents}{
	{\bfseries\centering\headingsize
		\setlength{\parskip}{0pt}
		\hspace*{-0.5\lmarg}\MakeUppercase\mytitle\par
		\vspace*{1em}
		\hspace*{-0.5\lmarg}INDEX\par
	}
	\@starttoc{toc}
}

% Descriptions
\RequirePackage{enumitem}
\setlist[description]{leftmargin=1cm,font=\bfseries\itshape}

% Subsections, etc.
\RequirePackage[at]{easylist}
\let\real@easylist\easylist
\let\real@endeasylist\endeasylist
\newif\ifineasylist\ineasylistfalse
\newcommand{\el@topstyle}[1]{(\headnum{List1})}
\renewenvironment{easylist}{\ineasylisttrue\setlength{\leftmargin}{-1cm}\real@easylist\NewList(Numbers=a,Numbers2=l,Numbers3=r,Numbers4=L,Numbers5=R,Hang=true,Progressive*=1cm,Align=1cm,Style*={(},Mark={)(},FinalMark={)},Style1*={\el@topstyle},Hide2=1,Hide3=2,Hide4=3,Hide5=4)}{\ineasylistfalse\real@endeasylist}
\newcommand{\undiv}{\ListProperties(Numbers=a,Numbers1=l,Numbers2=r,Numbers3=L,Numbers4=R)}

\newcommand{\elcont}{\advance\hangindent by -1cm\relax\par}
\newcommand{\elindent}{\addtolength{\leftmargin}{1cm}}

\newcounter{legallist}
\renewcommand{\thelegallist}{\ifnum\el@PreviousItem=1(\headnum{List1})\else(\csname el@NumberDenotation:\csname Numbers\el@PreviousItem\endcsname\endcsname{List\el@PreviousItem})\fi}
\RequirePackage{xpatch}
\xapptocmd{\elCreateItem}{\refstepcounter{legallist}\ignorespaces}{}{}

\RequirePackage{easylist-ref}

% Tables
\usepackage{longtable}
\usepackage{tabu}
\usepackage{booktabs}
\newcounter{rownumber}
\def\rownum{\noalign{\refstepcounter{rownumber}}\makebox[1.8em][l]{\headnum{rownumber}.}}
\AtBeginEnvironment{longtabu}{\setcounter{rownumber}{0}\footnotesize}

% Notes
\newlength\notetaglength
\newcommand{\note}[2]{\nopagebreak\par{\footnotesize\selectfont\settowidth{\notetaglength}{\textbf{#1:}}\addtolength{\notetaglength}{1em}\ifineasylist\hspace*{-\notetaglength}\fi\makebox[\notetaglength][l]{\textbf{#1:}}#2\addtolength{\hangindent}{\notetaglength}\par}}

\newcommand{\defn}[1]{\textit{\textbf{#1}}}

\AtBeginDocument{
	% Header/footer
	\fancypagestyle{plain}{
		\fancyhf{}
	}
	
	\pagestyle{fancy}
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt} % Remove lines
	
	\fancyheadoffset[loh,lof]{\lmarg}
	
	\lhead{\textsf{\scriptsize\mytitle}}
	\rhead{\textsf{\scriptsize\myauthor}}
	\lfoot{\textsf{\scriptsize\myfooter}}
	\rfoot{\textsf{\scriptsize\thepage}}
	
	\title{\mytitle}
	\author{\myauthor}
	
	\pagenumbering{roman}
	\tableofcontents
	\newpage
	\pagenumbering{arabic}
	\renewcommand{\thepage}{\headnum{page}}
}
